﻿
using FactoryPattern.Models;
using System;

namespace FactoryPattern
{
    public class Program
    {
        public class PizzaType
        {
            public const string pepperoni = "pepperoni";
            public const string greek = "greek";
            public const string mushrooms = "mushrooms";
            public const string cheese = "cheese";
        }

        static void Main(string[] args)
        {
            //ref
            //https://medium.com/@phayao/design-pattern-101-factory-pattern-a0a3f89cfc23

            var ingredient = new ChiangmaiPizzaIngredientFactory();
            var cm = new BangkokPizzaStore(ingredient);
            var pizza = cm.orderPizza(PizzaType.greek);
            var tik = "";
        }
    }
}
