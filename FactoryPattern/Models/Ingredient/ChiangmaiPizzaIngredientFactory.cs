﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Models
{
    public class ChiangmaiPizzaIngredientFactory : PizzaIngredientFactory
    {

        public Dough createDough()
        {
            return new ThinCrustDough();
        }

        public Sauce createSauce()
        {
            return new ChiangmaiSauce();
        }

        public Cheese createCheese()
        {
            return new ReggianoChesse();
        }
    }
}
