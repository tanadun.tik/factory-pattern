﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Models
{
    public interface PizzaIngredientFactory
    {
        Dough createDough();
        Sauce createSauce();
        Cheese createCheese();
    }
}
