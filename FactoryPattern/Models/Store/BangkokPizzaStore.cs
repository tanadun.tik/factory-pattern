﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Models
{
    public class BangkokPizzaStore : PizzaStore
    {
        public BangkokPizzaStore(PizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }
    }
}
