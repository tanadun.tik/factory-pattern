﻿using System;
using System.Collections.Generic;
using System.Text;
using static FactoryPattern.Program;

namespace FactoryPattern.Models
{
    public abstract class PizzaStore
    {
        protected PizzaIngredientFactory ingredientFactory;

        public Pizza orderPizza(String type)
        {
            Pizza pizza = createPizza(type);
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();

            return pizza;
        }

        public virtual Pizza createPizza(String type)
        {
            Pizza pizza = null;

            if (type.Equals(PizzaType.pepperoni))
            {
                pizza = new PepperoniPizza(ingredientFactory);
            }
            else if (type.Equals(PizzaType.greek))
            {
                pizza = new GreekPizza(ingredientFactory);
            }
            else if (type.Equals(PizzaType.mushrooms))
            {
                pizza = new MushroomsPizza(ingredientFactory);
            }
            else if (type.Equals(PizzaType.cheese))
            {
                pizza = new CheesePizza(ingredientFactory);
            }

            return pizza;
        }
    }
}
