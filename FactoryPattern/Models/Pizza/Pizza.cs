﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Models
{
    public abstract class Pizza
    {

        public virtual void prepare()
        {

        }
        public void bake()
        {

        }
        public void cut()
        {

        }
        public void box()
        {

        }
    }

    public class PepperoniPizza : Pizza
    {
        Dough dough;
        Sauce sauce;
        Cheese cheese;

        PizzaIngredientFactory ingredientFactory;

        public PepperoniPizza(PizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }



        public override void prepare()
        {
            dough = ingredientFactory.createDough();
            sauce = ingredientFactory.createSauce();
            cheese = ingredientFactory.createCheese();
        }
    }

    public class GreekPizza : Pizza
    {
        Dough dough;
        Sauce sauce;
        Cheese cheese;

        PizzaIngredientFactory ingredientFactory;

        public GreekPizza(PizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }



        public override void prepare()
        {
            dough = ingredientFactory.createDough();
            sauce = ingredientFactory.createSauce();
            cheese = ingredientFactory.createCheese();
        }
    }

    public class MushroomsPizza : Pizza
    {
        Dough dough;
        Sauce sauce;
        Cheese cheese;

        PizzaIngredientFactory ingredientFactory;

        public MushroomsPizza(PizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }



        public override void prepare()
        {
            dough = ingredientFactory.createDough();
            sauce = ingredientFactory.createSauce();
            cheese = ingredientFactory.createCheese();
        }
    }

    public class CheesePizza : Pizza
    {
        Dough dough;
        Sauce sauce;
        Cheese cheese;

        PizzaIngredientFactory ingredientFactory;

        public CheesePizza(PizzaIngredientFactory ingredientFactory)
        {
            this.ingredientFactory = ingredientFactory;
        }



        public override void prepare()
        {
            dough = ingredientFactory.createDough();
            sauce = ingredientFactory.createSauce();
            cheese = ingredientFactory.createCheese();
        }


    }

}
